﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public static class Tools{


    //Usefull UI tools
    public static IEnumerator CanvasFadeIn(CanvasGroup canvasGroup, float duration){
        for(float t=0f; t<1f; t+=Time.deltaTime/duration){
            canvasGroup.alpha = t;
            yield return null;
        }
    }

    public static IEnumerator CanvasFadeOut(CanvasGroup canvasGroup, float duration){
        
        for(float t=0f; t<1f; t+=Time.deltaTime/duration){
            canvasGroup.alpha = 1-t;
            yield return null;
        }
    }

    //Scene loader
    public static void LoadMainScene(){
        SceneManager.LoadScene("Main");
        SceneManager.sceneLoaded += SwitchToMainScene;
    }

    public static void UnsubsrcibeOnLoad(){
        SceneManager.sceneLoaded -= SwitchToMainScene;
    }

    private static void SwitchToMainScene(Scene scene, LoadSceneMode mode){
        Camera.main.GetComponent<OnMainSceneLoad>().Load();
    }

    public static void ReloadMainScene(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    //Persistent data storage
    public enum FleetType{DRONE, FIGHTER, FRIGATE};

    static private List<FleetType> playerUnits;
    static private List<FleetType> enemyUnits;

    static private bool useCustomBalance = false;
    static private int maxDeployedShips = 3;




    public static void SetPlayerUnits(List<FleetType> newList){playerUnits = newList;}

    public static List<FleetType> GetPlayerUnits(){return playerUnits;}

    public static void SetEnemyUnits(List<FleetType> newList){enemyUnits = newList;}

    public static List<FleetType> GetEnemyUnits(){return enemyUnits;}

    public static void SetUseCustomBalance(bool use){useCustomBalance = use;}
    
    public static bool GetUseCustomBalance(){return useCustomBalance;}

    public static int GetMaxDeployedShips(){return maxDeployedShips;}

    public static void SetMaxDeployedShips(int newCount){maxDeployedShips = newCount;}
   
}
