using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{

    [Header("Needs assignment")]
    public float moveSpeed;
    public float scrollSpeed;
    public float shiftMultiplier;
    public GameObject warpEffect;
    public GameObject victoryScreenPrefab;
    public GameObject explosionEffect;
    public GameObject warpEffectLite;
    public Transform cameraRestriction;
    public GameObject blackOverlay;
    public GameObject enemyMothership;

    private bool gameConcluded = false;
    private bool playerWon;
    private Transform victoryScreen;
    private float cameraSpeedMultiplier;

    private Transform canvas;

    private MeshRenderer cameraBounds;


    void Start(){
        GetComponent<InputController>().enabled = false;
        cameraBounds = cameraRestriction.GetComponent<MeshRenderer>();

        StartCoroutine(StartCutscene());
    }

    void Update(){
        
        if(!gameConcluded){

            if(Input.GetKey(KeyCode.LeftShift)){
                cameraSpeedMultiplier = shiftMultiplier;
            } else{
                cameraSpeedMultiplier = 1;
            }

            
            if(cameraBounds.bounds.Contains(transform.position + moveSpeed * cameraSpeedMultiplier * new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")))){
                transform.position += moveSpeed * cameraSpeedMultiplier * new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            }

            if(cameraBounds.bounds.Contains(transform.position + scrollSpeed * cameraSpeedMultiplier * new Vector3(0, -Input.GetAxisRaw("Mouse ScrollWheel"), 0))){
                transform.position += scrollSpeed * cameraSpeedMultiplier * new Vector3(0, -Input.GetAxisRaw("Mouse ScrollWheel"), 0);
            }
            
            
        }
        
    }

    private IEnumerator StartCutscene(){
        StartCoroutine(Tools.CanvasFadeOut(blackOverlay.GetComponent<CanvasGroup>(), 1));
        yield return new WaitForSeconds(4.4f);
        GetComponent<InputController>().enabled = true;
        GetComponent<Animator>().enabled = false;


    }


    public void CommenceEndingSequence(bool playerWon){
        enemyMothership.GetComponent<EnemyIntelligence>().enabled = false;
        canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
        if(gameConcluded){
            return;
        }

        gameConcluded = true;
        canvas.GetChild(1).GetComponent<TimeScaleButtonBehaviour>().ClickPlay();

        GetComponent<InputController>().SelectObject(null, false);
        GetComponent<InputController>().enabled = false;

        for(int i=0; i<canvas.childCount; i++){
            canvas.transform.GetChild(i).gameObject.SetActive(false);
        }

        GetComponent<Camera>().cullingMask = (LayerMask.NameToLayer("HideDuringCutscene"));

        this.playerWon = playerWon;
        StartCoroutine(PlayCutscene());

    }

    private IEnumerator PlayCutscene(){
        yield return new WaitForSeconds(2);
        if(!playerWon){
            transform.position = new Vector3(-215f, 22f, -293f);
            transform.rotation = Quaternion.Euler(0f, 90f, 0f);
            Instantiate(warpEffect, new Vector3(90f, 20f, -225f), Quaternion.Euler(0f, -90f, 0));
            
            victoryScreen = Instantiate(victoryScreenPrefab, canvas).transform;
            Instantiate(explosionEffect, new Vector3(-150f, 17f, -264f), Quaternion.Euler(0, 0, 0));

            
            StartCoroutine(EnemyVictoryScenicCameraMovement(7));
            yield return new WaitForSeconds(2);
            yield return VictoryScreenFadeIn(3f);
            yield return new WaitForSeconds(0.5f);
            yield return VictoryTextFadeIn("DEFEAT", 3f);
            yield return VictoryButtonsFadeIn(1f);
            
        } else{
            transform.position = new Vector3(-140f, 35, -230);
            transform.rotation = Quaternion.Euler(0f, 27f, 0);

            victoryScreen = Instantiate(victoryScreenPrefab, canvas).transform;
            Instantiate(explosionEffect, new Vector3(150f, 17f, 264f), Quaternion.Euler(0, 180, 0));

            StartCoroutine(PlayerVictoryScenicCameraMovement(12));
            yield return new WaitForSeconds(1);
            Instantiate(warpEffectLite);
            yield return new WaitForSeconds(3);
            StartCoroutine(EnemyMothershipDepartureScene(0.2f));
            yield return new WaitForSeconds(3);
            yield return VictoryScreenFadeIn(3f);
            yield return new WaitForSeconds(0.5f);
            yield return VictoryTextFadeIn("VICTORY", 3f);
            yield return VictoryButtonsFadeIn(1f);
            


        }

        
    }

    private IEnumerator VictoryScreenFadeIn(float time){
        for(float t=0f; t<1f; t+=Time.deltaTime/time){
            Color newColor = new Color(0, 0, 0, t);
            victoryScreen.GetChild(0).GetComponent<Image>().color = newColor;
            yield return null;
        }
        
    }

    private IEnumerator VictoryTextFadeIn(string text, float time){
        victoryScreen.GetChild(1).GetComponent<Text>().text = text;
        for(float t=0f; t<1f; t+=Time.deltaTime/time){
            Color newColor = new Color(255, 255, 255, t);
            victoryScreen.GetChild(1).GetComponent<Text>().color = newColor;
            yield return null;
        }
    }

    private IEnumerator VictoryButtonsFadeIn(float time){
        for(float t=0f; t<1f; t+=Time.deltaTime/time){
            Color newColor = new Color(255, 255, 255, t);
            victoryScreen.GetChild(2).GetComponent<Text>().color = newColor;
            victoryScreen.GetChild(3).GetComponent<Text>().color = newColor;
            yield return null;
        }
        victoryScreen.GetChild(2).GetComponent<Button>().enabled = true;
        victoryScreen.GetChild(3).GetComponent<Button>().enabled = true;
    }
    
    private IEnumerator EnemyVictoryScenicCameraMovement(float time){
        for(float t=0f; t<1f; t+=Time.deltaTime/time){
            transform.position = Vector3.Lerp(new Vector3(-215f, 22f, -293f), new Vector3(-240f, 22f, -293f), t);
            yield return null;
        }
    }
    
    private IEnumerator PlayerVictoryScenicCameraMovement(float time){
        for(float t=0f; t<1f; t+=Time.deltaTime/time){
            transform.position = Vector3.Lerp(new Vector3(-140f, 35f, -230f), new Vector3(-160f, 35f, -230f), t);
            yield return null;
        }
    }

    private IEnumerator EnemyMothershipDepartureScene(float time){
        Transform mothership = GameObject.FindGameObjectWithTag("Enemy Mothership").transform;
        
        mothership.GetComponent<Mothership>().DisableDockedShips();


        for(float t=0f; t<1f; t+=Time.deltaTime/time){
            mothership.position = Vector3.Lerp(mothership.GetChild(0).transform.position, new Vector3(-800f, 0, 215), t);
            yield return null;
        }
    }

}
