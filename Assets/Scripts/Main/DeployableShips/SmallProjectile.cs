﻿using System;
using System.Collections;
using UnityEngine;
using Unity;

public class SmallProjectile : MonoBehaviour{

    [Header("Pre-assigned")]
    public GameObject explosionEffect;
    public float explosionEffectDuration;   //Duration in seconds
    public float speed;

    [Header("Auto-assigned at runtime")]
    private float damage;
    private float damageAdvantage;
    private string advantageAgainst;
    private MeshCollider[] entityColliders;  //Array of colliders, projectiles need to be set to ignore these
    private Transform target;
    private float range;
    private float scale;
    

    public void Init(float damage, float damageAdvantage, string advantageAgainst, MeshCollider[] entityColliders, SphereCollider clickCollider, Transform target, float range, float scale){
        this.damage = damage;
        this.damageAdvantage = damageAdvantage;
        this.advantageAgainst = advantageAgainst;
        this.range = range;
        this.scale = scale;

        transform.localScale = new Vector3(scale, scale, scale);

        Physics.IgnoreCollision(clickCollider, this.GetComponent<MeshCollider>());

         foreach(MeshCollider collider in entityColliders){
            Physics.IgnoreCollision(collider, this.GetComponent<MeshCollider>());
        }
        Physics.IgnoreCollision(GameObject.FindGameObjectWithTag("Playing Field").GetComponent<MeshCollider>(), this.GetComponent<MeshCollider>());


        if(target.tag.Equals("Enemy Ship") || target.tag.Equals("Player Ship")){
            
            int randomNumberx = new System.Random().Next(0, target.GetComponent<DeployableShip>().entityPositions.Length);
            this.transform.LookAt(target.position + target.GetComponent<DeployableShip>().entityPositions[randomNumberx]);
            

        } else if(target.tag.Equals("Enemy Mothership") || target.tag.Equals("Player Mothership")){

            transform.LookAt(target.GetChild(3).transform.position);
        
        } else {
            Debug.LogError("Error while creating a projectile: Unexpected target.");
        }

        GetComponent<Rigidbody>().velocity = transform.forward*speed;

        StartCoroutine(DestroyInTime());





    }

    private void Explode(){

        GameObject effect = Instantiate(explosionEffect, transform.position, transform.rotation);
        effect.transform.localScale = new Vector3(scale*3, scale*3, scale*3);
        Destroy(effect, explosionEffectDuration);   //Destroyes explosion after certain time (effectDuration)

        Destroy(gameObject);



    }

    private IEnumerator DestroyInTime(){

        yield return new WaitForSeconds(range/speed);

        Explode();
    }


    private void OnTriggerEnter(Collider collider){


        if(collider == null){
            return;
        }

        if(collider.tag.Equals("Player Ship") || collider.tag.Equals("Enemy Ship")){
            Physics.IgnoreCollision(this.GetComponent<MeshCollider>(), collider.transform.GetComponent<SphereCollider>());
            return;
        }

        if(collider.tag.Equals("Projectile")){
            Physics.IgnoreCollision(this.GetComponent<MeshCollider>(), collider.transform.GetComponent<MeshCollider>());
            return;
        }

        if(collider.tag.Equals("Untagged")){
            Explode();
        }

        if(collider.tag.Equals("Asteroid")){
            Explode();
        }

        Explode();

        try{

            if(collider.transform.parent.tag.Equals("Enemy Mothership") || collider.transform.parent.tag.Equals("Player Mothership")){
                collider.transform.parent.GetComponent<Mothership>().ReceiveDamage(damage);

            } else if(collider.transform.parent.tag.Equals("Player Ship") || collider.transform.parent.tag.Equals("Enemy Ship")){
                
                if(collider.transform.parent.GetComponent<DeployableShip>().type.Equals(advantageAgainst)){
                    collider.transform.parent.GetComponent<DeployableShip>().ReceieveDamage(damage+(damage*(damageAdvantage/100)));

                } else{
                    collider.transform.parent.GetComponent<DeployableShip>().ReceieveDamage(damage);
                }

            } else{
                Debug.Log("A projectile hit something unusual, eg. another projectile O_o");
            }



        } catch(System.Exception){

        }
        






    }





}
