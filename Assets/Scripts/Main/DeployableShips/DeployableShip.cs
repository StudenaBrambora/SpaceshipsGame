﻿using System;
using UnityEngine;
using UnityEngine.AI;

public abstract class DeployableShip : MonoBehaviour
{

    public enum shipType {DRONE, FIGHTER, FRIGATE};
    public enum recallStatus {FREE, RETURNING, APPROACHING, PARKING, STOWED, DEPARTING};

    //Values
    [Header("Pre-assigned")]
    public UnityEngine.Object projectile;
    public float projectileScale;
    public UnityEngine.Object explosionEffect;
    public float explosionScale;
    public bool isPlayer;

    [Header("Auto-assigned at runtime")]
    public bool controllable;

    public shipType type;
    public bool isPatched;   //Has already been destroyed once
    public int entityCount;
    public float healthMax;
    public float healthActual;

    protected float damage;
    protected float damageAdvantage;
    protected string advantageAgainst;
    protected float reloadTime;
    
    public float range;
    protected float speed;
    protected float angularSpeed;
    protected float acceleration;

    public Transform target;
    
    protected float timer = 0.0f;
    public Vector3[] entityPositions;
    protected float singleEntityDamage;

    protected MeshCollider[] entityColliders;

    public Transform uiButton; //UI button for the ship, assigned by the button itself
    public Transform mothership;

    private recallStatus state = recallStatus.STOWED;

    public bool autoKite = false;  //Valid for player only, AI always kites


    //Components
    protected NavMeshAgent leaderNavAgent;    //Handles movement
    protected LineRenderer line;    //Displays movement path

    private LineRenderer attackLine;
    private SphereCollider clickCollider;



    //Constructor but not constructor
    protected void Init(int entC){
        leaderNavAgent = GetComponent<NavMeshAgent>();
        clickCollider = GetComponent<SphereCollider>();
        line = GetComponent<LineRenderer>();
        line.SetPosition(1, transform.position);    //Sets path to current position

        if(isPlayer){
            attackLine = transform.GetChild(0).GetComponent<LineRenderer>();
            attackLine.SetPosition(0, transform.position);
        }
        

        entityCount = entC;
        singleEntityDamage = damage/entityCount;

        entityPositions = new Vector3[entityCount];
        entityColliders = new MeshCollider[entityCount];

        for(int i=0; i<entityCount; i++){
            if(i==0){
                leaderNavAgent.speed = speed;
                leaderNavAgent.angularSpeed = angularSpeed;
                leaderNavAgent.acceleration = acceleration;
            } else{
                this.gameObject.transform.GetChild(i).GetComponent<NavMeshAgent>().speed = speed;   //Nav mesh config
                this.gameObject.transform.GetChild(i).GetComponent<NavMeshAgent>().angularSpeed = angularSpeed;
                this.gameObject.transform.GetChild(i).GetComponent<NavMeshAgent>().acceleration = acceleration;
            }
            entityPositions[i] = this.gameObject.transform.GetChild(i).transform.localPosition;
            entityColliders[i] = this.gameObject.transform.GetChild(i).GetComponent<MeshCollider>();

        }

    }

    protected void Start(){
        try{
            this.GetComponent<SmallHealthbar>().Init();
            this.GetComponent<SmallHealthbar>().SetValue(healthActual, healthMax);

            this.GetComponent<RangeIndicator>().Init();
            this.GetComponent<RangeIndicator>().SetRange(range);
        } catch (Exception){
            Debug.LogError("Error while initializing a deployable ship: No healthbar or range indicator scripts found.");
        }
        controllable = false;

    }

    protected abstract void Awake();

    protected virtual void Update(){

        if(state != recallStatus.FREE && state != recallStatus.DEPARTING && state != recallStatus.STOWED){
            RecallSequence();
        } else if(state == recallStatus.DEPARTING){
            DeploySequence();
        }

        if(isPlayer){
            DrawPath();
            DrawAttackMarker();
            if(target != null && autoKite){
                AutoKite();
            }
        }
        
        if(IsReadyToFire()){
            Fire();
        }
    }

    //Methods
    protected virtual void Move(Vector3 targetPosition, bool changeState){               //Move the ship to a destination
        if(changeState){
            state = recallStatus.FREE;
        }
        
        
        for(int i=0; i<entityCount; i++){
            if(i==0){
                leaderNavAgent.destination = targetPosition;
            } else{
                this.gameObject.transform.GetChild(i).GetComponent<NavMeshAgent>().destination = targetPosition + entityPositions[i];
            }

        }
    }

    public virtual void Move(Vector3 targetPosition){
        Move(targetPosition, true);
    }

    protected void DrawPath(){                              //Render line, where ship is about to move
        line.SetPosition(0, transform.position);
        if(leaderNavAgent.path.corners.Length < 2){   //If the path is straight forward
            return;
        }
        line.positionCount = leaderNavAgent.path.corners.Length;

        for(int i = 1; i < leaderNavAgent.path.corners.Length; i++){
            line.SetPosition(i, leaderNavAgent.path.corners[i]);
        }
    }

    private void DrawAttackMarker(){    //For Player only
        attackLine.SetPosition(0, transform.position);
        if(target != null){
            attackLine.SetPosition(1, target.position);
        } else{
            attackLine.SetPosition(1, transform.position);
        }

    }

    public void ReceieveDamage(float incomingDamage){
        healthActual -= incomingDamage;

        if(healthActual <= 0){
            healthActual = 0;

            for(int i=0; i<entityCount; i++){
                GameObject explosion = Instantiate(explosionEffect, transform.GetChild(i).position, Quaternion.Euler(0, 0, 0)) as GameObject;
                explosion.transform.localScale = new Vector3(explosionScale, explosionScale, explosionScale);
                Destroy(explosion, 2.0f);
                autoKite = false;
            }

            mothership.GetComponent<Mothership>().RecallShip(gameObject);
            state = recallStatus.STOWED;
            controllable = false;
            isPatched = true;
            



        } else if(healthActual >= healthMax){
            healthActual = healthMax;
        } else if(isPatched && healthActual > healthMax/2){
            healthActual = healthMax/2;
        }

        try{
            this.GetComponent<SmallHealthbar>().SetValue(healthActual, healthMax);   //Tells healthbar to update
        }catch(Exception){
            Debug.LogError("Exception while updating ship's healtbar: No healthbar script found");
        }

        if(!isPlayer){  //For AI only
            TargetClosestEnemy();
        }



    }

    protected void Fire(){
        for(int i=0; i<entityCount; i++){
            GameObject spawnedProjectile = Instantiate(projectile, transform.GetChild(i).position, Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
            spawnedProjectile.GetComponent<SmallProjectile>().Init(damage, damageAdvantage, advantageAgainst, entityColliders, clickCollider, target, range, projectileScale);


        }
        
    }

    protected bool IsReadyToFire(){
        timer += Time.deltaTime;
        if(timer >= reloadTime && target != null){

            if(target.tag.Equals("Enemy Mothership") || target.tag.Equals("Player Mothership")){

                if(target.GetChild(1).GetComponent<MeshRenderer>().bounds.Contains(this.transform.position)){
                    timer = 0.0f;
                    return true;
                }


            } else{

                //Is target in range
                foreach(Vector3 pos in target.GetComponent<DeployableShip>().entityPositions){
                    if(Vector3.Distance(transform.position, target.position + pos) <= range){
                        timer = 0.0f;
                        return true;
                    }
                }

            }


            return false;
        } else{
            return false;
        }
    }

    public void CommenceRecallSequence(){
        if(state != recallStatus.FREE){
            return ;
        }
        state = recallStatus.RETURNING;
        Move(mothership.GetComponent<Mothership>().outerRecallWaypoint.position, false);
        RecallSequence();
    }
    
    private void RecallSequence(){

        switch(state){
            case recallStatus.RETURNING:
                if(mothership.GetChild(4).GetComponent<MeshRenderer>().bounds.Contains(transform.position)){
                    state = recallStatus.APPROACHING;
                    Move(mothership.GetComponent<Mothership>().outerRecallWaypoint.position, false);
                    controllable = false;
                    ColliderSetActive(false);
                }
                break;

            case recallStatus.APPROACHING:
                if(Vector3.Distance(transform.position, mothership.GetComponent<Mothership>().outerRecallWaypoint.position) <= 5){
                    state = recallStatus.PARKING;
                    Move(mothership.GetComponent<Mothership>().innerRecallWaypoint.position, false);
                } else{
                    Move(mothership.GetComponent<Mothership>().outerRecallWaypoint.position, false);
                }
                break;

            case recallStatus.PARKING:
                if(Vector3.Distance(transform.position, mothership.GetComponent<Mothership>().innerRecallWaypoint.position) <= 5){
                    state = recallStatus.STOWED;
                    mothership.GetComponent<Mothership>().RecallShip(gameObject);
                } else{
                    Move(mothership.GetComponent<Mothership>().innerRecallWaypoint.position, false);
                }
                break;
        }
        
    }

    public void CommenceDeployment(){
        if(state != recallStatus.STOWED){
            return;
        }
        state = recallStatus.DEPARTING;
        DeploySequence();
    }

    private void DeploySequence(){
        if(Vector3.Distance(transform.position, mothership.GetComponent<Mothership>().outerRecallWaypoint.position) <= 5){
            state = recallStatus.FREE;
            if(tag.Equals("Player Ship")){
                controllable = true;
                ColliderSetActive(true);
            }
            
        } else {
            Move(mothership.GetComponent<Mothership>().outerRecallWaypoint.position, false);
        }
    }

    public void Warp(Vector3 targetPosition){
        for(int i=0; i<entityCount; i++){
            if(i==0){
                leaderNavAgent.Warp(targetPosition);
            } else{
                transform.GetChild(i).GetComponent<NavMeshAgent>().Warp(targetPosition + entityPositions[i]);
            }

        }
    }

    public void NavMeshAgentEnabled(bool enabled){
        for(int i=0; i<entityCount; i++){
            if(i==0){
                leaderNavAgent.enabled = enabled;
                this.gameObject.GetComponent<LineRenderer>().enabled = enabled;
            } else{
                this.gameObject.transform.GetChild(i).GetComponent<NavMeshAgent>().enabled = enabled;
            }

        }
    }

    public string[] getInfoForUI(){
        string[] temp = new string[5];

        temp[0] = type.ToString();
        temp[1] = (int)Math.Round(healthActual) + "/" + (int)Math.Round(healthMax);
        temp[2] = ((int)Math.Round((damage / reloadTime))).ToString();
        temp[3] = ((int)Math.Round(speed)).ToString();
        if(target != null){
            temp[4] = target.ToString();
        }
        
        

        return temp;
    }

    public shipType GetShipType(){
        return type;
    }

    private void TargetClosestEnemy(){  //For AI only

        GameObject closest = null;
        float distance = Mathf.Infinity;

        foreach(GameObject go in GameObject.FindGameObjectWithTag("Player Mothership").GetComponent<Mothership>().GetDeployedShips()){
            if(Vector3.Distance(go.transform.position, transform.position) < distance){
                closest = go;
                distance = Vector3.Distance(go.transform.position, transform.position);
            }
        }

        if(closest != null){
            target = closest.transform;
        }
        
    }

    private void ColliderSetActive(bool enable){
        for(int i=0; i<entityCount; i++){
            transform.GetChild(i).GetComponent<MeshCollider>().enabled = enable;
        }
    }

    private void AutoKite(){
        if(Vector3.Distance(transform.position, target.transform.position) < range -10){
            Move(-transform.forward);
        } else if(Vector3.Distance(transform.position, target.transform.position) > range){
            Move(target.transform.position);
        }
    }

    

}
