﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("DeployableShipData")]
public class DeployableShipFromXML
{
    
    public class DeployableShipType{
        [XmlAttribute("type")]
        public string type;
        public float health;
        public float damage;
        public float damageAdvantage;
        public string advantageAgainst;
        public float reloadTime;
        public float range;
        public float speed;
        public float angularSpeed;
        public float acceleration;
    }

    [XmlArray("DeployableShipTypes"), XmlArrayItem("DeployableShip")]
    public DeployableShipType[] deployableShipTypes;

    public static DeployableShipFromXML Load(){
        if(Tools.GetUseCustomBalance()){

            try{

                var serializer = new XmlSerializer(typeof(DeployableShipFromXML));
                using(var stream = new FileStream("Spaceships_Data/Resources/DeployableShipInfo.xml", FileMode.Open)){
                return serializer.Deserialize(stream) as DeployableShipFromXML;
                }


            } catch(System.Exception){
                Debug.LogError("Spaceships_Data/Resources/DeployableShipInfo.xml not found, using default values instead...");

                
                XmlSerializer serializer = new XmlSerializer(typeof(DeployableShipFromXML));

                TextAsset xml = Resources.Load("DeployableShipInfo") as TextAsset;
                StringReader reader = new StringReader(xml.text);

                DeployableShipFromXML items = serializer.Deserialize(reader) as DeployableShipFromXML;

                return items;

            }

            


        } else{

            XmlSerializer serializer = new XmlSerializer(typeof(DeployableShipFromXML));

            TextAsset xml = Resources.Load("DeployableShipInfo") as TextAsset;
            StringReader reader = new StringReader(xml.text);

            DeployableShipFromXML items = serializer.Deserialize(reader) as DeployableShipFromXML;

            return items;

        }


        

    }


}