﻿using UnityEngine;
using UnityEngine.AI;


public class FighterSquadron : DeployableShip
{

    
    protected override void Awake(){
        type = shipType.FIGHTER;
        isPatched = false;

        DeployableShipFromXML ships = DeployableShipFromXML.Load();

        DeployableShipFromXML.DeployableShipType info = null;
        for(int i = 0; i < ships.deployableShipTypes.Length; i++){
            if(ships.deployableShipTypes[i].type.Equals("FIGHTER")){
                info = ships.deployableShipTypes[i];
                break;
            }
        }
        
        healthMax = info.health;
        healthActual = healthMax;

        damage = info.damage;
        damageAdvantage = info.damageAdvantage;
        advantageAgainst = info.advantageAgainst;
        reloadTime = info.reloadTime;

        range = info.range;
        speed = info.speed;
        angularSpeed = info.angularSpeed;
        acceleration = info.acceleration;

        Init(3);

        leaderNavAgent.speed = speed;   //Must be assigned after DeployableShip.init(int entityCount) has been called, otherwise will crash!!!
        leaderNavAgent.angularSpeed = angularSpeed;
        leaderNavAgent.acceleration = acceleration;
    }
}