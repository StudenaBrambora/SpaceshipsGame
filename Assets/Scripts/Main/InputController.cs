﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputController : MonoBehaviour
{

    [Header("Needs assignment")]
    public Transform pauseText;
    public Transform gameSpeedButton;
    public Transform pauseMenu;
    

    [Header("Auto-assigned at runtime")]
    public GameObject selectedObject;

    private bool pausedMenuEnabled = false;
    private float timeScale;




    void Start(){
        selectedObject = null;
        timeScale = 1;
    }


    void Update(){

        if(!pausedMenuEnabled){
            selectedObject = CatchSelectCommand();
            CatchMoveCommand();
            CatchPauseCommand();
            CatchTimeScaleChangeCommand();
        }

        CatchESCMenuCommand();


    }



    public GameObject SelectObject(GameObject newObject, bool overwriteDirectly){
        if(selectedObject != null){
            selectedObject.GetComponent<RangeIndicator>().Hide();
        }

        if(newObject != null && (newObject.tag.Equals("Player Ship") || newObject.tag.Equals("Enemy Ship"))){
            newObject.GetComponent<RangeIndicator>().Show();
        }

        if(overwriteDirectly){
            selectedObject = newObject;
        }

        return newObject;
    }

    private GameObject CatchSelectCommand(){

        if (Input.GetMouseButtonDown(0)){

            try{
                if(EventSystem.current.currentSelectedGameObject.tag.Equals("UIInfoBox")){
                    return selectedObject;
                }

            } catch(System.Exception){}
            

            RaycastHit hitInfo = new RaycastHit();
            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo)){
                
                string objectTag = "";
                string parentTag = "";

                try{
                    parentTag = hitInfo.transform.parent.gameObject.tag;
                } catch(System.Exception){}
                objectTag = hitInfo.transform.tag;
               

                if(parentTag.Equals("Player Ship") || parentTag.Equals("Enemy Ship")){
                    return SelectObject(hitInfo.transform.parent.gameObject, false);
                } else if(objectTag.Equals("Player Ship") || objectTag.Equals("Enemy Ship")){
                    return SelectObject(hitInfo.transform.gameObject, false);
                } else {
                    return SelectObject(null, false);
                }

            } else {
                return SelectObject(null, false);
            }

        }
        return selectedObject;
    }

    private void CatchMoveCommand(){
        if(Input.GetMouseButtonDown(1) && selectedObject != null){
            if(selectedObject.GetComponent<DeployableShip>().controllable){



                RaycastHit hitInfo = new RaycastHit();
                if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo)){

                    if(hitInfo.transform.tag.Equals("Asteroid")){return;}
                    if(hitInfo.transform.tag.Equals("Player Ship")){return;}

                    //Order movement
                    if(hitInfo.transform.tag.Equals("Playing Field")){
                        Vector3 newPosition = hitInfo.point;
                        newPosition.y = 0;
                        selectedObject.GetComponent<DeployableShip>().Move(newPosition);
                    }

                    //Order attack
                    if(hitInfo.transform.tag.Equals("Enemy Ship")){
                        selectedObject.GetComponent<DeployableShip>().target = hitInfo.transform;  //Assigns new target to selected ship
                    } else if(hitInfo.transform.parent.tag.Equals("Enemy Mothership")){
                        selectedObject.GetComponent<DeployableShip>().target = hitInfo.transform.parent;
                    } else if(hitInfo.transform.parent.tag.Equals("Enemy Ship")){
                        selectedObject.GetComponent<DeployableShip>().target = hitInfo.transform.parent;
                    }
                }
            }

        }
    }

    private void CatchPauseCommand(){
        if(Input.GetKeyDown(KeyCode.Space) && Time.timeScale != 0){
            gameSpeedButton.GetComponent<TimeScaleButtonBehaviour>().ClickPause();
        } else if(Input.GetKeyDown(KeyCode.Space) && Time.timeScale == 0){
            gameSpeedButton.GetComponent<TimeScaleButtonBehaviour>().ClickPlay();
        }
    }

    private void CatchTimeScaleChangeCommand(){
        if(Input.GetKeyDown(KeyCode.Alpha1)){
            gameSpeedButton.GetComponent<TimeScaleButtonBehaviour>().ClickPause();
            timeScale = 0;
        } else if(Input.GetKeyDown(KeyCode.Alpha2)){
            gameSpeedButton.GetComponent<TimeScaleButtonBehaviour>().ClickPlay();
            timeScale = 1;
        } else if(Input.GetKeyDown(KeyCode.Alpha3)){
            gameSpeedButton.GetComponent<TimeScaleButtonBehaviour>().ClickFast();
            timeScale = 2;
        }
    }

    private void CatchESCMenuCommand(){
        if(Input.GetKeyDown(KeyCode.Escape) && !pausedMenuEnabled){
            Pause();
        } else if(Input.GetKeyDown(KeyCode.Escape) && pausedMenuEnabled){
            Resume();
        }
    }

    public void Pause(){
        timeScale = Time.timeScale;
        Time.timeScale = 0;
        pausedMenuEnabled = true;
        pauseMenu.gameObject.SetActive(true);
    }

    public void Resume(){
        Time.timeScale = timeScale;
        pausedMenuEnabled = false;
        pauseMenu.gameObject.SetActive(false);
    }


}
