﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIntelligence : MonoBehaviour{

    private List<GameObject> playerStowedShips;
    private List<GameObject> playerDeployedShip;

    private List<GameObject> enemyStowedShips;
    private List<GameObject> enemyDeployedShips;

    private Mothership mothershipScript;

    private GameObject playerMothership;

    private bool gameStarted = false;


    void Start(){
        enemyStowedShips = GetComponent<Mothership>().GetRecalledShips();
        enemyDeployedShips = GetComponent<Mothership>().GetDeployedShips();

        playerStowedShips = GameObject.FindGameObjectWithTag("Player Mothership").GetComponent<Mothership>().GetRecalledShips();
        playerDeployedShip = GameObject.FindGameObjectWithTag("Player Mothership").GetComponent<Mothership>().GetDeployedShips();

        mothershipScript = GetComponent<Mothership>();

        playerMothership = GameObject.FindGameObjectWithTag("Player Mothership");

        StartCoroutine(FirstDeploy());
        

    }

    void Update(){
        if(gameStarted){
            GiveOrders();
            DeployShipsIfPossible(false);

        }
        


    }




    //Tools
    private DeployableShip.shipType GetCounter(GameObject ship){
        switch(ship.GetComponent<DeployableShip>().GetShipType()){
            case DeployableShip.shipType.DRONE:
                return DeployableShip.shipType.FIGHTER;
            
            case DeployableShip.shipType.FIGHTER:
                return DeployableShip.shipType.FRIGATE;

            case DeployableShip.shipType.FRIGATE:
                return DeployableShip.shipType.DRONE;

            default:
                Debug.LogError("Error at AI ship recognition: Unknown ship type detected!");
                return DeployableShip.shipType.DRONE;
        }

    }

    private bool DeployShipByType(DeployableShip.shipType shipType){
        if(enemyStowedShips.Count > 0){
            foreach(GameObject ship in enemyStowedShips){
                if(ship.GetComponent<DeployableShip>().GetShipType().Equals(shipType)){
                    mothershipScript.DeployShip(ship);
                    return true;
                }
            }
        }
        return false;
    }

    private void AttackShip(GameObject attacker, GameObject target){
        attacker.GetComponent<DeployableShip>().target = target.transform;
        

        if(Vector3.Distance(attacker.transform.position, target.transform.position) < attacker.GetComponent<DeployableShip>().range -10){
            attacker.GetComponent<DeployableShip>().Move(-transform.forward);
        } else if(Vector3.Distance(attacker.transform.position, target.transform.position) > attacker.GetComponent<DeployableShip>().range){
            attacker.GetComponent<DeployableShip>().Move(target.transform.position);
        }

    }

    private void AttackMothership(GameObject attacker){
        attacker.GetComponent<DeployableShip>().target = playerMothership.transform;
        attacker.GetComponent<DeployableShip>().Move(playerMothership.transform.GetChild(1).GetComponent<MeshRenderer>().bounds.center);
    }

    private GameObject CheckAttckZoneForEnemies(){
        foreach(GameObject ship in playerDeployedShip){
            if(transform.GetChild(2).GetComponent<MeshRenderer>().bounds.Contains(ship.transform.position)){
                return ship;
            }
        }
        return null;



    }

    private bool RetreatIfNeeded(GameObject ship, bool forced){
        if(GetEffectiveHealth(ship) < 0.3){
            ship.GetComponent<DeployableShip>().CommenceRecallSequence();
            return true;
        } else{
            return false;
        }
    }

    private float GetEffectiveHealth(GameObject ship){
        float healthActual = ship.GetComponent<DeployableShip>().healthActual;
        float hMax;
        if(ship.GetComponent<DeployableShip>().isPatched){
            hMax = ship.GetComponent<DeployableShip>().healthMax/2;
        } else{
            hMax = ship.GetComponent<DeployableShip>().healthMax;
        }

        return healthActual/hMax;

    }






    //States
    private IEnumerator FirstDeploy(){

        yield return new WaitForSeconds(6);

        if(playerDeployedShip.Count > 0){
            foreach(GameObject ship in playerDeployedShip){
                DeployShipByType(GetCounter(ship));
            }
            

            for(int i = 0; enemyDeployedShips.Count < Tools.GetMaxDeployedShips() && i < enemyStowedShips.Count; ){
                mothershipScript.DeployShip(enemyStowedShips[i]);
            }
            

        } else{
            for(int i = 0; enemyDeployedShips.Count < Tools.GetMaxDeployedShips() && i < enemyStowedShips.Count; ){
                mothershipScript.DeployShip(enemyStowedShips[i]);
            }
        }

        yield return new WaitForSeconds(3);
        gameStarted = true;

    }

    private void GiveOrders(){

        GameObject temp = CheckAttckZoneForEnemies();

        //Charge mothership
        //All ships will take advantage of the empty battlefield to deal as much damage to opponent's mothership as possible
        if(playerDeployedShip.Count == 0){
            foreach(GameObject ship in enemyDeployedShips){
                AttackMothership(ship);
            }
        } else

        //Defend mothership
        //All ships will go defend mothership
        if(temp != null){
            if(enemyDeployedShips.Count > 0){
                foreach(GameObject ship in enemyDeployedShips){
                    AttackShip(ship, temp);
                }
            } else{
                DeployShipsIfPossible(true);
            }

            
        } else

        //If number superiority is achieved
        //Each opponent's ship will be intercepted by countering (if possible) ship, the rest will attack the mothership
        if(playerDeployedShip.Count < enemyDeployedShips.Count){
            List<GameObject> unassignedShips = new List<GameObject>();
            unassignedShips.AddRange(enemyDeployedShips);
            

            foreach(GameObject playerShip in playerDeployedShip){

                foreach(GameObject enemyShip in unassignedShips){
                    if(enemyShip.GetComponent<DeployableShip>().Equals(GetCounter(playerShip))){
                        AttackShip(enemyShip, playerShip);
                        unassignedShips.Remove(enemyShip);
                        break;
                    }
                }

                AttackShip(unassignedShips[0], playerShip);
                unassignedShips.Remove(unassignedShips[0]);

            }

            foreach(GameObject ship in unassignedShips){
                if(!RetreatIfNeeded(ship, false)){
                    AttackMothership(ship);
                }
                
            }


        } else


        //If the opponent has the number superiority
        //All ships will take on one enemy together until superiory is achieved
        if(playerDeployedShip.Count > enemyDeployedShips.Count){
            foreach(GameObject enemyShip in enemyDeployedShips){
                AttackShip(enemyShip, playerDeployedShip[0]);
            }
        } else


        //Stalemate in terms of numbers
        if(playerDeployedShip.Count == enemyDeployedShips.Count){

            List<GameObject> unassignedShips = new List<GameObject>();
            unassignedShips.AddRange(enemyDeployedShips);

            foreach(GameObject playerShip in playerDeployedShip){

                foreach(GameObject enemyShip in unassignedShips){
                    if(enemyShip.GetComponent<DeployableShip>().Equals(GetCounter(playerShip))){
                        AttackShip(enemyShip, playerShip);
                        unassignedShips.Remove(enemyShip);
                        break;
                    }
                }

                AttackShip(unassignedShips[0], playerShip);
                unassignedShips.Remove(unassignedShips[0]);

            }

            foreach(GameObject ship in unassignedShips){
                AttackShip(ship, playerDeployedShip[0]);
            }


        }





        




    }

    private void DeployShipsIfPossible(bool emergency){
        if(enemyDeployedShips.Count < Tools.GetMaxDeployedShips() && enemyStowedShips.Count != 0){

            GameObject highestHealth = null;
            
            foreach(GameObject ship in enemyStowedShips){
                if(ship.GetComponent<DeployableShip>().healthActual == ship.GetComponent<DeployableShip>().healthMax){
                    mothershipScript.DeployShip(ship);
                    break;
                } else if(GetEffectiveHealth(ship) > 0.5){
                    highestHealth = ship;
                    if(emergency){
                        mothershipScript.DeployShip(ship);
                        break;
                    }
                }
            }

            if(highestHealth != null && enemyDeployedShips.Count < Tools.GetMaxDeployedShips()){
                mothershipScript.DeployShip(highestHealth);
            }            
        }
    }
    
}
