﻿using UnityEngine;

public class SmallHealthbar : MonoBehaviour
{

    [Header("Pre-assigned")]
    public Transform canvasPrefab;
    public Vector3 canvasPosition;

    private Transform canvas;
    private float redBarWidth;

    [Header("Auto-assigned at runtime")]
    public bool updateParity = true;


    public void Init(){

        canvas = Instantiate(canvasPrefab, this.transform);
        canvas.transform.position = this.transform.position + canvasPosition;
        
        redBarWidth = canvas.transform.GetChild(1).GetComponent<RectTransform>().rect.width;

    }

    void Update(){
        
        canvas.transform.rotation = Quaternion.Euler(75.0f, 0.0f, 0.0f);
        canvas.transform.position = this.transform.position + canvasPosition;
    }

    public void SetValue(float healthActual, float healthMax){
        float newBarWidth = redBarWidth - ((healthActual/healthMax)*redBarWidth);
        canvas.GetChild(1).GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, newBarWidth);
    
        updateParity = !updateParity;
    }


}
