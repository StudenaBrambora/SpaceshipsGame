﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MothershipHealthbarBehaviour : MonoBehaviour{
   
   public Transform mothership;

   private bool updateParity = false;

   void Start(){


   }

   void Update(){
       try{
           if(mothership.GetComponent<Mothership>().updateParity != updateParity){
           SetValue(mothership.GetComponent<Mothership>().healthActual, mothership.GetComponent<Mothership>().healthMax);
        }
       } catch(System.Exception){
           
       }
       


   }

   private void SetValue(float healthActual, float healthMax){
        if(healthActual < 0){
            healthActual = 0;
        }


        float newBarWidth = (healthActual/healthMax);
        transform.GetChild(2).GetComponent<RectTransform>().localScale = new Vector3(newBarWidth, 1, 1);
        //transform.GetChild(2).GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, newBarWidth);

        updateParity = !updateParity;

   }



}
