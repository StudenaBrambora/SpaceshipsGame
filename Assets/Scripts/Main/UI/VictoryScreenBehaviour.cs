﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryScreenBehaviour : MonoBehaviour{

    public void ClickRestart(){
        Tools.ReloadMainScene();
    }

    public void ClickReturn(){
        Tools.UnsubsrcibeOnLoad();
        SceneManager.LoadScene("Menu");
    }
   
}
