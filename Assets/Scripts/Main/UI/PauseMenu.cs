﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour{

    private InputController inputScript;

    void Start(){
        inputScript = Camera.main.GetComponent<InputController>();
    }


    public void ClickRestart(){
        Time.timeScale = 1;
        Tools.ReloadMainScene();
    }

    public void ClickReturn(){
        Tools.UnsubsrcibeOnLoad();
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    public void ClickResume(){
        inputScript.Resume();
    }

    public void ClickExit(){
        Application.Quit();
    }
   
}
