using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedInfoBoxBehaviour : MonoBehaviour
{

    private string[] selectedObjectInfo;

    private Text shipTypeText;
    private Text healthText;
    private Text dpsText;
    private Text speedText;
    private Text targetText;
    private Toggle kiteToggle;

    private Transform mainCamera;

    void Awake(){

        shipTypeText = transform.GetChild(2).GetComponent<Text>();
        healthText = transform.GetChild(3).GetComponent<Text>();
        dpsText = transform.GetChild(4).GetComponent<Text>();
        speedText = transform.GetChild(5).GetComponent<Text>();
        targetText = transform.GetChild(6).GetComponent<Text>();
        kiteToggle = transform.GetChild(7).GetComponent<Toggle>();

        mainCamera = Camera.main.transform;

    }

    void Update(){
        if(mainCamera.GetComponent<InputController>().selectedObject != null){

            if((mainCamera.GetComponent<InputController>().selectedObject.tag == "Player Ship") || mainCamera.GetComponent<InputController>().selectedObject.tag == "Enemy Ship"){
                selectedObjectInfo = mainCamera.GetComponent<InputController>().selectedObject.GetComponent<DeployableShip>().getInfoForUI();
            
                shipTypeText.text = selectedObjectInfo[0];
                healthText.text = "Health: " + selectedObjectInfo[1];
                dpsText.text = "DPS: " + selectedObjectInfo[2];
                speedText.text = "Speed: " + selectedObjectInfo[3];
                targetText.text = "Target: " + selectedObjectInfo[4];
                
                kiteToggle.SetIsOnWithoutNotify(mainCamera.GetComponent<InputController>().selectedObject.GetComponent<DeployableShip>().autoKite);

            } else{
            shipTypeText.text = mainCamera.GetComponent<InputController>().selectedObject.ToString();
            healthText.text = "Health: " ;
            dpsText.text = "DPS: ";
            speedText.text = "Speed: ";
            targetText.text = "Target: ";
            } 

        } else{
            shipTypeText.text = "";
            healthText.text = "Health: " ;
            dpsText.text = "DPS: ";
            speedText.text = "Speed: ";
            targetText.text = "Target: ";

        } 

    }
 
    public void ClickToggle(){
        Debug.Log("clicked");

        if(mainCamera.GetComponent<InputController>().selectedObject != null){
            mainCamera.GetComponent<InputController>().selectedObject.GetComponent<DeployableShip>().autoKite = !mainCamera.GetComponent<InputController>().selectedObject.GetComponent<DeployableShip>().autoKite;
        }
        
       



        
        
    }

}
