using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScaleButtonBehaviour : MonoBehaviour
{

    public enum timeControls {PAUSE, PLAY, FAST};

    private Transform pauseButton;
    private Transform playButton;
    private Transform fastButton;

    private Color32 unselectedColor;
    private Color32 selectedColor;

    [Header("Needs assignment")]
    public Transform pauseText;
    

    void Start(){

        pauseButton = transform.GetChild(8).transform;
        playButton = transform.GetChild(5).transform;
        fastButton = transform.GetChild(2).transform;

        unselectedColor = new Color32(15, 0, 185, 255);
        selectedColor = new Color32(255, 255, 255, 255);
        playButton.GetComponent<Image>().color = selectedColor;
        
    }


    public void ClickPause(){

        pauseButton.GetComponent<Image>().color = selectedColor;
        playButton.GetComponent<Image>().color = unselectedColor;
        fastButton.GetComponent<Image>().color = unselectedColor;

        Time.timeScale = 0;
        pauseText.gameObject.SetActive(true);
    }

    public void ClickPlay(){
        pauseButton.GetComponent<Image>().color = unselectedColor;
        playButton.GetComponent<Image>().color = selectedColor;
        fastButton.GetComponent<Image>().color = unselectedColor;

        Time.timeScale = 1;
        pauseText.gameObject.SetActive(false);

    }

    public void ClickFast(){
        pauseButton.GetComponent<Image>().color = unselectedColor;
        playButton.GetComponent<Image>().color = unselectedColor;
        fastButton.GetComponent<Image>().color = selectedColor;

        Time.timeScale = 2;
        pauseText.gameObject.SetActive(false);

    }

}
