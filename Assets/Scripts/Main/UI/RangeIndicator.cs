﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeIndicator : MonoBehaviour
{
    
    [Header("Needs manual assignment")]
    public Transform canvasPrefab;
    public Vector3 canvasPosition;

    private Transform canvas;

    [Header("Auto-assigned at runtime")]
    public GameObject ship;

    private float size;


    public void Init(){
        canvas = Instantiate(canvasPrefab, this.transform);
        canvas.transform.position = this.transform.position + canvasPosition;
        Hide();
    }


    void Update(){

        canvas.transform.rotation = Quaternion.Euler(90.1f, 0.0f, 0.0f);
        canvas.transform.position = this.transform.position + canvasPosition;
    }

    public void SetRange(float newRange){
        size = newRange*2.4f;
        
        canvas.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size);
        canvas.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size);
    }

    public void Hide(){
        canvas.gameObject.SetActive(false);
    }

    public void Show(){
        canvas.gameObject.SetActive(true);
    }



}
