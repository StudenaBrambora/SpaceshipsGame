﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDButtonBehaviour : MonoBehaviour
{

    [Header("Auto-assigned at runtime")]
    public Transform ship;


    private float healthBarWidth;
    private bool updateParity = false;

    

    void Start(){
        
        healthBarWidth = transform.GetChild(2).GetComponent<RectTransform>().rect.width;

        transform.GetChild(4).gameObject.SetActive(false);   //Recall text
        transform.GetChild(5).gameObject.SetActive(true);  //Deploy text

    }

    void Update(){
        if(ship.GetComponent<SmallHealthbar>().updateParity != updateParity){
            SetValue(ship.GetComponent<DeployableShip>().healthActual, ship.GetComponent<DeployableShip>().healthMax);
            if(ship.GetComponent<DeployableShip>().isPatched){
                ChangeToOrange();
            }
        }



    }

    private void SetValue(float healthActual, float healthMax){
        float newBarWidth = (healthActual/healthMax)*healthBarWidth;
        
        transform.GetChild(2).GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, newBarWidth);

        updateParity = !updateParity;
    }

    public void SetAsSelected(){    //Triggered by button component
        Camera.main.GetComponent<InputController>().SelectObject(ship.gameObject, true);
    }

    public void ClickRecall(){      //Triggered by button component
        ship.GetComponent<DeployableShip>().CommenceRecallSequence();
    }

    public void ClickDeploy(){      //Triggered by button component
        ship.GetComponent<DeployableShip>().mothership.GetComponent<Mothership>().DeployShip(ship.gameObject);
    }

    public void ChangeToDeploy(){
        transform.GetChild(4).gameObject.SetActive(false);   //Recall text
        transform.GetChild(5).gameObject.SetActive(true);  //Deploy text
    }

    public void ChangeToRecall(){
        transform.GetChild(4).gameObject.SetActive(true);   //Recall text
        transform.GetChild(5).gameObject.SetActive(false);  //Deploy text
    }

    public void ChangeToOrange(){
        transform.GetChild(2).GetComponent<Image>().color = new Color32(255, 222, 0, 255);
    }






}
