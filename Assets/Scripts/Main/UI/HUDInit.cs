﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDInit : MonoBehaviour
{

    private enum mothershipType{PLAYER, ENEMY};

    [Header("Pre-assigned")]
    public Transform droneButtonPrefab;
    public Transform fighterButtonPrefab;
    public Transform frigateButtonPrefab;
    public Transform playerMothershipHealthbarPrefab;
    public Transform enemyMothershipHealthPrefab;
    public Transform selectedInfoBoxPrefab;


    [Header("Auto-assigned at runtime")]
    private GameObject[] playerShips;

    void Start(){

        playerShips = GameObject.FindGameObjectsWithTag("Player Ship");


        int counter = 1;
        foreach(GameObject ship in playerShips){

            CreateButton(ship, counter);
            counter++;
        }

        CreateMothershipHealthbar(GameObject.FindGameObjectWithTag("Player Mothership").transform, true);
        CreateMothershipHealthbar(GameObject.FindGameObjectWithTag("Enemy Mothership").transform, false);

        Instantiate(selectedInfoBoxPrefab, transform);


    }

    private void CreateMothershipHealthbar(Transform mothership, bool isPlayer){

        Transform newHealthbar;

        if(isPlayer){
            newHealthbar = Instantiate(playerMothershipHealthbarPrefab, transform);
        } else{
            newHealthbar = Instantiate(enemyMothershipHealthPrefab, transform);
        }

        newHealthbar.GetComponent<MothershipHealthbarBehaviour>().mothership = mothership;


    }


    private void CreateButton(GameObject ship, int position){

        Transform newButton;

        switch(ship.GetComponent<DeployableShip>().type.ToString()){
            case "DRONE":
                newButton = Instantiate(droneButtonPrefab, transform);
                break;

            case "FIGHTER":
                newButton = Instantiate(fighterButtonPrefab, transform);
                break;

            case "FRIGATE":
                newButton = Instantiate(frigateButtonPrefab, transform);
                break;
            
            default:
                Debug.LogError("Unexpected case while creating a ship selection button: Unknown ship type.");
                newButton = null;
                break;
        }

        try{
            newButton.GetComponent<HUDButtonBehaviour>().ship = ship.transform;
            newButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(50, (position*115)+180, 0);

        } catch (System.Exception){
            Debug.LogError("Exception while assigning ship to button: Button is missing a script.");
        }

        ship.GetComponent<DeployableShip>().uiButton = newButton;

    }


}
