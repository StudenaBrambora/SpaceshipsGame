﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMainSceneLoad : MonoBehaviour{

    public GameObject playerDronePrefab;
    public GameObject playerFighterPrefab;
    public GameObject playerFrigatePrefab;

    public GameObject enemyDronePrefab;
    public GameObject enemyFighterPrefab;
    public GameObject enemyFrigatePrefab;

    public GameObject asteroidPrefab;



    public void Load(){


        int asteroidNumber = Random.Range(2, 10);
        float posX;
        float posZ;

        float rotX;
        float rotY;
        float rotZ;

        float scale;

        for(int i=0; i<=asteroidNumber; i++){
            posX = Random.Range(-220.0f, 220.0f);
            posZ = Random.Range(-70.0f, 70.0f);

            rotX = Random.Range(0.0f, 360.0f);
            rotY = Random.Range(0.0f, 360.0f);
            rotZ = Random.Range(0.0f, 360.0f);

            scale = Random.Range(6.0f, 12.0f);

            Instantiate(asteroidPrefab, new Vector3(posX, 0, posZ), Quaternion.Euler(rotX, rotY, rotZ)).transform.localScale = new Vector3(scale, scale, scale);

        }


        List<Tools.FleetType> playerUnits = Tools.GetPlayerUnits();
        List<Tools.FleetType> enemyUnits = Tools.GetEnemyUnits();


        foreach(Tools.FleetType unit in playerUnits){
            switch(unit){
                case Tools.FleetType.DRONE:
                    Instantiate(playerDronePrefab).name = "Drone";
                    break;

                case Tools.FleetType.FIGHTER:
                    Instantiate(playerFighterPrefab).name = "Fighter";
                    break;

                case Tools.FleetType.FRIGATE:
                    Instantiate(playerFrigatePrefab).name = "Frigate";
                    break;
                
            }
        }

        foreach(Tools.FleetType unit in enemyUnits){
            switch(unit){
                case Tools.FleetType.DRONE:
                    Instantiate(enemyDronePrefab).name = "Drone";
                    break;

                case Tools.FleetType.FIGHTER:
                    Instantiate(enemyFighterPrefab).name = "Fighter";
                    break;

                case Tools.FleetType.FRIGATE:
                    Instantiate(enemyFrigatePrefab).name = "Frigate";
                    break;
                
            }
        }
    }

    
}
