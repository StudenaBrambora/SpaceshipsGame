﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Mothership : MonoBehaviour
{

    
    [Header("Pre-assigned")]
    public float healthMax;
    public int fieldCapacity;
    public bool isPlayer;

    public float healingRate;
    public float healDelay;

    public Vector3 recallPosition;
    public Vector3 deployPosition;

    [Header("Auto-assigned at runtime")]
    public GameObject[] playerShips;
    private List<GameObject> recalledShips = new List<GameObject>();
    private List<GameObject> deployedShips = new List<GameObject>();

    public float healthActual;
    public bool updateParity = true;

    private float healTimer;

    public Transform outerRecallWaypoint;
    public Transform innerRecallWaypoint;

    private int animationPhase = 0;


    void Start(){
        innerRecallWaypoint = transform.GetChild(8);
        outerRecallWaypoint = transform.GetChild(9);

        healTimer = 0.0f;

        healthActual = healthMax;

        if(isPlayer){
            playerShips = GameObject.FindGameObjectsWithTag("Player Ship");
        } else {
            playerShips = GameObject.FindGameObjectsWithTag("Enemy Ship");
        }

        
        foreach(GameObject ship in playerShips){
            ship.GetComponent<DeployableShip>().mothership = this.transform;

            RecallShip(ship, true);

        }

        

    }

    void Update(){
    
        if(healTimer >= healDelay){
            RepairDockedShips(healingRate);
            healTimer = 0;
        }
        healTimer += Time.deltaTime;
        

        
    }

    public void ReceiveDamage(float damage){
        healthActual -= damage;
        if(healthActual <= 0){
            
            Camera.main.GetComponent<CameraController>().CommenceEndingSequence(!isPlayer);
            return;
        }

        switch(animationPhase){
            case 0:
                if(healthActual/healthMax <= 0.80f){
                    animationPhase = 1;
                    //Phase1
                    for(int i=0; i<transform.GetChild(10).GetChild(1).childCount; i++){
                        transform.GetChild(10).GetChild(1).GetChild(i).GetComponent<ParticleSystem>().Play(true);
                    }

                }
                break;

            case 1:
                if(healthActual/healthMax <= 0.60f){
                    animationPhase = 2;
                    //Phase2
                    for(int i=0; i<transform.GetChild(10).GetChild(2).childCount; i++){
                        transform.GetChild(10).GetChild(2).GetChild(i).GetComponent<ParticleSystem>().Play(true);
                    }
                }
                break;

            case 2:
                if(healthActual/healthMax <= 0.40f){
                    animationPhase = 3;
                    //Phase3
                    for(int i=0; i<transform.GetChild(10).GetChild(3).childCount; i++){
                        transform.GetChild(10).GetChild(3).GetChild(i).GetComponent<ParticleSystem>().Play(true);
                    }
                }
                break;

            case 3:
                if(healthActual/healthMax <= 0.20f){
                    animationPhase = 4;
                    //Phase4
                    for(int i=0; i<transform.GetChild(10).GetChild(4).childCount; i++){
                        transform.GetChild(10).GetChild(4).GetChild(i).GetComponent<ParticleSystem>().Play(true);
                    }
                }
                break;


        }

        updateParity = !updateParity;
    }


    public void RecallShip(GameObject ship, bool gameInit){
        //TODO
        recalledShips.Add(ship);
        
        ship.GetComponent<DeployableShip>().NavMeshAgentEnabled(false);
        ship.GetComponent<DeployableShip>().Warp(new Vector3(recallPosition.x, ship.transform.position.y, recallPosition.z));

        if(!gameInit){  //Button is not yet initialized when this function runs for the first time
            if(isPlayer){
                ship.GetComponent<DeployableShip>().uiButton.GetComponent<HUDButtonBehaviour>().ChangeToDeploy();
            }
            
            deployedShips.Remove(ship);
        }

        


    }

    public void RecallShip(GameObject ship){
        RecallShip(ship, false);
    }

    public void DeployShip(GameObject ship){


        //TODO

        if(deployedShips.Count == Tools.GetMaxDeployedShips()){
            return;
        }

        deployedShips.Add(ship);
        recalledShips.Remove(ship);

        
        ship.GetComponent<DeployableShip>().Warp(new Vector3(innerRecallWaypoint.position.x, ship.transform.position.y, innerRecallWaypoint.position.z));
        ship.GetComponent<DeployableShip>().NavMeshAgentEnabled(true);

        //ship.GetComponent<DeployableShip>().Move(new Vector3(deployPosition.x, ship.transform.position.y, deployPosition.z + 30));

        try{
            ship.GetComponent<DeployableShip>().uiButton.GetComponent<HUDButtonBehaviour>().ChangeToRecall();
        } catch(System.Exception){
            
        }

        ship.GetComponent<DeployableShip>().CommenceDeployment();
        
    

    }


    private void RepairDockedShips(float amount){
        foreach(GameObject ship in recalledShips){

            ship.GetComponent<DeployableShip>().ReceieveDamage(- amount);

        }
    }

    public void DisableDockedShips(){
        foreach(GameObject ship in recalledShips){
            ship.SetActive(false);
        }
    }

    public List<GameObject> GetRecalledShips(){
        return this.recalledShips;
    }

    public List<GameObject> GetDeployedShips(){
        return deployedShips;
    }

}
