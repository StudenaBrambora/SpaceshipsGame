﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractiveRulesMap : MonoBehaviour{

    private Text descriptionText;

    void Start(){
        descriptionText = transform.GetChild(2).GetChild(0).GetComponent<Text>();
    }

    public void ResetText(){
        descriptionText.text = "Click the map to the left to learn about different parts of the battlefield.";
    }

    public void ClickEnemyMothership(){
        descriptionText.text = "The enemy mothership.\nForce it to retreat by inflicting enough damage. Once gone, the enemy fleets won't stand a chance.";
    }

    public void ClickPlayerMothership(){
        descriptionText.text = "Your mothership.\nYou can store and repair your fleets here as well as command them. Protect at all costs!";
    }

    public void ClickEnemyDeploy(){
        descriptionText.text = "The enemy deploy zone.\nOpponent's fleets come in and out of here.";
    }

    public void ClickPlayerDeploy(){
        descriptionText.text = "Your deploy zone.\nYour fleets must converge here before they are allowed to retreat in to the safety of the mothership.";
    }
    
    public void ClickEnemyAttack(){
        descriptionText.text = "Enemy attack zone.\nHere, the enemy mothership is most vulnerable and any fleets here can target the mothership itself.";
    }

    public void ClickPlayerAttack(){
        descriptionText.text = "Your attack zone.\nAny fleet capable of reaching this area can target and damage your mothership. Do not let that happen!";
    }

    public void ClickBattlefield(){
        descriptionText.text = "The battlefield.\nHere, blood will be spilt. Expect no mercy...";
    }


}
