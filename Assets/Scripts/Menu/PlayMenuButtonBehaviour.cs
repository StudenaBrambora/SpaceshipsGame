﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMenuButtonBehaviour : MonoBehaviour{
    
    private PlayMenuBehaviour parentScript;

    void Start(){
        parentScript = transform.parent.GetComponent<PlayMenuBehaviour>();
    }

    public void OnClick(){
        parentScript.ClickExisting();
    }

}
