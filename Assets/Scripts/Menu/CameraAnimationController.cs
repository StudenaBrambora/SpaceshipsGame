﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CameraAnimationController : MonoBehaviour{

    public Transform mainMenu;
    public Transform warpEffectTransform;
    public Transform playMenu;
    public Transform blackOverlay;


    private Animator animator;
    private ParticleSystem warpEffect;
    private CanvasGroup blackOverlayCanvas;


    void Awake(){
        
        
    }

    void Start(){
        animator = GetComponent<Animator>();
        warpEffect = warpEffectTransform.GetComponent<ParticleSystem>();

        blackOverlayCanvas = blackOverlay.GetComponent<CanvasGroup>();
        blackOverlayCanvas.alpha = 1;
        StartCoroutine(Tools.CanvasFadeOut(blackOverlayCanvas, 1));



    }

    public void ClickPlay(){
        mainMenu.GetComponent<GraphicRaycaster>().enabled = false;
        animator.SetInteger("state", 1);
        EventSystem.current.SetSelectedGameObject(null);
        warpEffect.Play();

        StartCoroutine(ChangeToPlayScreen());
        
    }

    private IEnumerator ChangeToPlayScreen(){
        yield return new WaitForSeconds(4);

        yield return Tools.CanvasFadeIn(blackOverlayCanvas, 2);

        playMenu.gameObject.SetActive(true);

        yield return Tools.CanvasFadeOut(blackOverlayCanvas, 2);

        
        Destroy(warpEffectTransform.gameObject);
        
        mainMenu.GetComponent<GraphicRaycaster>().enabled = false;
        
    }

    public void ClickRules(){
        animator.SetInteger("state", 2);
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void ClickAbout(){
        animator.SetInteger("state", 3);
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void ClickExit(){
        Application.Quit();
    }

    public void ClickReturn(){
        animator.SetInteger("state", 0);
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void ReloadMenu(){
        animator.SetInteger("state", 0);
        SceneManager.LoadScene("Menu");
        StartCoroutine(Tools.CanvasFadeOut(blackOverlayCanvas, 1));
    }


    public void ClickStartGame(){
        StartCoroutine(StartLoadingMain());
    }

    private IEnumerator StartLoadingMain(){
        if(Tools.GetPlayerUnits().Count > 0 && Tools.GetEnemyUnits().Count > 0){
            
            yield return Tools.CanvasFadeIn(blackOverlayCanvas, 1);
        
            Tools.LoadMainScene();

        }

        yield return null;
        
    }



 


}
