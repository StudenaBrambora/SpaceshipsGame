﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayMenuBehaviour : MonoBehaviour{

    public bool isPlayer;
    public GameObject droneButtonPrefab;
    public GameObject fighterButtonPrefab;
    public GameObject frigateButtonPrefab;
    public GameObject sliderText;

    private List<GameObject> list;

    void Start(){
        list = new List<GameObject>();
    }

    public void ClickExisting(){
        list.Remove(EventSystem.current.currentSelectedGameObject);
        Destroy(EventSystem.current.currentSelectedGameObject);
        UpdatePositions();
        
    }

    public void ClickAddDrone(){
        
        if(list.Count == 6){
            return;
        }

        GameObject newButton = Instantiate(droneButtonPrefab, transform);
        newButton.name = "Drone";

        list.Add(newButton);
        UpdatePositions();

        
    }


    public void ClickAddFighter(){
        if(list.Count == 6){
            return;
        }

        GameObject newButton = Instantiate(fighterButtonPrefab, transform);
        newButton.name = "Fighter";

        list.Add(newButton);
        UpdatePositions();

        
    }

    public void ClickAddFrigate(){
        if(list.Count == 6){
            return;
        }

        GameObject newButton = Instantiate(frigateButtonPrefab, transform);
        newButton.name = "Frigate";

        list.Add(newButton);
        UpdatePositions();

        
    }

    private void UpdatePositions(){
        EventSystem.current.SetSelectedGameObject(null);

        for(int i=0; i<list.Count; i++){
            list[i].transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, -50-(i*100), 0);
        }

    }

    public void ClickToggle(){
        Tools.SetUseCustomBalance(!Tools.GetUseCustomBalance());
    }

    public void ClickPlay(){

        List<Tools.FleetType> toSend = new List<Tools.FleetType>();
        foreach(GameObject unit in list){
            switch(unit.name){
                case "Drone":
                    toSend.Add(Tools.FleetType.DRONE);
                    break;

                case "Fighter":
                    toSend.Add(Tools.FleetType.FIGHTER);
                    break;

                case "Frigate":
                    toSend.Add(Tools.FleetType.FRIGATE);
                    break;
                
            }
        }
        

        if(isPlayer){
            Tools.SetPlayerUnits(toSend);
        } else{
            Tools.SetEnemyUnits(toSend);
        }
    }

    public void MoveSlider(){
        try{
            sliderText.GetComponent<Text>().text = EventSystem.current.currentSelectedGameObject.GetComponent<Slider>().value.ToString();
            Tools.SetMaxDeployedShips((int) EventSystem.current.currentSelectedGameObject.GetComponent<Slider>().value);
        } catch(System.Exception){}
        


    }
    
}
