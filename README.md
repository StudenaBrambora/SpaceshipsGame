# Spaceships
Spaceships is a simple real-time strategy game, where the player takes command of a fleet of strike crafts against an AI opponent.

## Features
- RTS spaceship combat in the middle of an asteroid field.
- Reactive artificial opponent.
- Configurable spaceship roster to create your own difficulty.
- Easy modification of spaceships stats to alter your gaming experience.
- Time control.

## Rule summary
The player takes control of multiple combat space craft docked in a mothership carrier. The goal of the game is to destroy the opposing mothership by navigating one's fleet through a randomly arranged asteroid field. Each space craft type is a bit different and may be more effective against other certain type. It is important to pick your fights wisely!

## Preview
![alt text](https://gitlab.com/StudenaBrambora/SpaceshipsGame/-/raw/master/Preview/Spaceships.png "Spaceships")
![alt text](https://gitlab.com/StudenaBrambora/SpaceshipsGame/-/raw/master/Preview/Spaceships2.png "Spaceships")
![alt text](https://gitlab.com/StudenaBrambora/SpaceshipsGame/-/raw/master/Preview/Spaceships_Rules.png "Spaceships")

## Notes
This project has been created as a high school graduation project at SPŠ Purkyňova and represents the best of my ability in the field of programming at the time.

The playable standalone game build can be found on [GitLab](https://gitlab.com/StudenaBrambora/spaceshipsbuild "Spaceships Build").
Special thanks goes to:
- RNDr. Lenka Hrušková (graduation project consultant)
- Ing. Petr Pernes (graduation project opponent)
- David Longín (sprites)
- Joseph Adam Saunders (feedback and playtesting)
- Petr Novotný (feedback and playtesting)
